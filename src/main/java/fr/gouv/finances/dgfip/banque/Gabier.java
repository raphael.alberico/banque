package fr.gouv.finances.dgfip.banque;

public class Gabier {

    private SystemeBancaireInterface systemeBancaire;

    public Gabier(SystemeBancaireInterface maBanque) {
        this.systemeBancaire = maBanque;
    }

    public String accesCompte(String numCarte, String codePin)
            throws SystemeBancaireException {
        if (this.systemeBancaire.verifierCodePin(numCarte, codePin)) {
            return this.systemeBancaire.rechercherRIBCompteCarte(numCarte);
        }
        return null;
    }

    public int retirerEspeces(String ribCompte, double montant)
            throws SystemeBancaireException {
        // TODO Auto-generated method stub
        this.systemeBancaire.creerOperation(ribCompte, "Retrait gabier",
                -montant);
        return 0;
    }

}
