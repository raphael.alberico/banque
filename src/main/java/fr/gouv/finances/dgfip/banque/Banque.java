package fr.gouv.finances.dgfip.banque;

import java.util.HashSet;
import java.util.Set;

public class Banque implements SystemeBancaireInterface {

//    private static final String Expiration = null;

    protected String codeBanque;

//    private int compteur = 0;
    private Integer compteur = 0;

    public Banque(String codeBanque) {
        this.codeBanque = codeBanque;
    }

    private Set<CompteBancaire> compteBancaires = new HashSet<CompteBancaire>();
    private Set<CarteBancaire> carteBancaires = new HashSet<CarteBancaire>();

    public CompteCourant creerCompteCourant(Personne personne,
            String codeGuichet, double soldeInitial) {
        CompteCourant c = new CompteCourant(codeBanque, codeGuichet,
                soldeInitial);
        c.setNumCompte(compteur.toString());
        compteur += 1;

        c.setTitulaire(personne);

        compteBancaires.add(c);
        return c;

    }

    public CompteCourant creerCompteCourant(Personne personne,
            String codeGuichet) {
        return this.creerCompteCourant(personne, codeGuichet, 0.);
    }

    public CompteEpargne creerCompteEpargne(Personne personne,
            String codeGuichet, double soldeInitial, double taux) {
        CompteEpargne c = new CompteEpargne(codeBanque, codeGuichet,
                soldeInitial, taux);
        c.setNumCompte(compteur.toString());
        compteur += 1;

        c.setTitulaire(personne);

        compteBancaires.add(c);
        return c;
    }

    public void afficherSyntheseComptes() {
        System.out.println(
                "+-----------------+--------------------------+----------------------+------------+\n"
                        + "| Type compte     | RIB                      | Titulaire            | Solde      |\n"
                        + "+-----------------+--------------------------+----------------------+------------+");
        for (CompteBancaire c : compteBancaires) {
            String typeCompte = c.getClass().getSimpleName().equals(
                    "CompteEpargne") ? "Compte epargne" : "Compte courant";
            String RIB = c.getRib();
            String titulaire = c.getTitulaire().getNom() + " "
                    + c.getTitulaire().getPrenom();
            String solde = c.solde.toString();

            System.out.println(String.format("| %-15s | %s | %-20s | %10s |",
                    typeCompte, RIB, titulaire, solde));
        }
        System.out.println(
                "+-----------------+--------------------------+----------------------+------------+");
    }

    public String getCodeBanque() {
        return codeBanque;
    }

    public void setCodeBanque(String codeBanque) {
        this.codeBanque = codeBanque;
    }

    public CarteBancaire creerCarte(Personne titulaire, CompteCourant compte) {
        String numCarte = "1234 5678 9101 1213";
        String codePin = "888";
        CarteBancaire c = new CarteBancaire(codePin, numCarte);
        c.setTitulaire(titulaire);
        c.setCompteCourant(compte);

        carteBancaires.add(c);
        return c;

    }

    public String CarteBancaire(String CodePin, String numCarte,
            String Expiration) {
        return "CarteBancaire [numCarte=" + numCarte + ", codePin=" + CodePin
                + ", Expiration=" + Expiration + "]";
    }

    public void afficherSyntheseCartes() {
        // for (CarteBancaire c : carteBancaires) {
        // System.out.println(c);
        // }

        System.out.println(
                "+----------------------+----------------------+----------+\n"
                        + "| Titulaire            | Num. Carte           | Code PIN |\n"
                        + "+----------------------+----------------------+----------+");

        for (CarteBancaire c : carteBancaires) {
            String titulaire = c.getTitulaire().getNom() + " "
                    + c.getTitulaire().getPrenom();
            String numCarte = c.numCarte.toString();
            String codePin = c.codePin.toString();

            System.out.println(String.format("| %-20s | %-20s | %8s |",
                    titulaire, numCarte, codePin));
        }
        System.out.println(
                "+----------------------+----------------------+----------+");
    }

    @Override
    public String rechercherRIBCompteCarte(String numCarte)
            throws SystemeBancaireException {

        CarteBancaire cbFound = searchCarte(numCarte);

        CompteCourant cc = cbFound.getCompteCourant();
        return cc.getRib();
    }

    @Override
    public Integer creerOperation(String ribCompte, String libelle,
            Double montant) throws SystemeBancaireException {
        // TODO Auto-generated method stub
        CompteBancaire compteFound = null;
        for (CompteBancaire c : compteBancaires) {
            if (c.getRib().equals(ribCompte)) {
                compteFound = c;
                break;
            }
        }

        if (compteFound == null) {
            throw new SystemeBancaireException("Compte not found");
        }

        try {
            return compteFound.creerOperation(libelle, montant);
        } catch (Exception e) {
            System.err.println(e.getMessage());
//            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Boolean verifierCodePin(String numCarte, String codePin)
            throws SystemeBancaireException {

        CarteBancaire cbFound = searchCarte(numCarte);

        return cbFound.verifierPin(codePin);
    }

    private CarteBancaire searchCarte(String numCarte)
            throws SystemeBancaireException {
        CarteBancaire cbFound = null;
        for (CarteBancaire cb : carteBancaires) {
            if (cb.numCarte.equals(numCarte)) {
                cbFound = cb;
                break;
            }
        }

        if (cbFound == null) {
            throw new SystemeBancaireException("Carte not found.");
        }
        return cbFound;
    }

}
