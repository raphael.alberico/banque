package fr.gouv.finances.dgfip.banque;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public abstract class CompteBancaire {

    private static final fr.gouv.finances.dgfip.banque.CompteCourant cc = null;

    private static CompteCourant CompteCourant = cc;

    protected String codeBanque;
    protected String codeGuichet;
    protected String numCompte;
    protected String cle;
    protected Double solde;

    List<Operation> listeOperation = new ArrayList<Operation>();

    private int numeroOperation = 0;

    public void setNumCompte(String numCompte) {
        this.numCompte = numCompte;
    }

    private Personne titulaire;

    public void setTitulaire(Personne titulaire) {
        this.titulaire = titulaire;
    }

    public Personne getTitulaire() {
        return titulaire;
    }

    protected CompteCourant creerCompteCourant(String codeBanque,
            String PersonneTitulaire, String CodeGuichet, String NumCompte,
            double Soldeinitial) {
        return CompteCourant;
    };

    public int creerOperation(String libelle, Double montant) throws Exception {
        Operation p = new Operation(libelle, montant);
        int toReturn = numeroOperation;
        p.setNumOperation(numeroOperation);
        numeroOperation += 1;
        listeOperation.add(p);
        this.solde += montant;
        return toReturn;
    }

    public String getRib() {
        return String.format("%s %s %09d %3s", this.codeBanque,
                this.codeGuichet, Integer.parseInt(this.numCompte), this.cle);
    }

    public void afficherSyntheseOperations() {

        String head = "+---------+-------------------------+-------------------------+------------+\n"
                + "| Num opé | Date opération          | Libellé                 | Montant    |\n"
                + "+---------+-------------------------+-------------------------+------------+";

        System.out.println(head);

        for (Operation p : listeOperation) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                    "dd/MM/yyyy");
            String dateOperation = simpleDateFormat.format(p.dateOperation);

            System.out.println(String.format("| %7d | %-23s | %23s | %10.2f |",
                    p.numOperation, dateOperation, p.libelle, p.montant));
        }
        System.out.println(
                "+---------+-------------------------+-------------------------+------------+");

    }

}
