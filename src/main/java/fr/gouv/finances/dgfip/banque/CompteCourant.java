package fr.gouv.finances.dgfip.banque;

public class CompteCourant extends CompteBancaire {

    private Object operations;

    public CompteCourant(String codeBanque, String codeGuichet,
            double soldeInitial) {
        this.codeGuichet = codeGuichet;
        this.solde = soldeInitial;
        this.codeBanque = codeBanque;
        this.cle = "99";
    }

    @Override
    public int creerOperation(String libelle, Double montant) throws Exception {
        if (this.solde + montant < -1000) {
            throw new Exception("Découvert pas possible");
        } else {
            return super.creerOperation(libelle, montant);
        }
    }

    @Override
    public String toString() {
        return "CompteCourant [titulaire=" + this.getTitulaire()
                + ",  codeBanque=" + codeBanque + ", codeGuichet=" + codeGuichet
                + ", numCompte=" + numCompte + ", cle=" + cle + ", solde="
                + solde + "]";
    }

    public Double calculerSolde() {
        // System.out.println("calculerSolde");
        Double solde = this.solde;
        {
            solde += 1;
        }
        return solde;
    }

    public Object getOperations() {
        return operations;
    }

    public void setOperations(Object operations) {
        this.operations = operations;
    }

}
