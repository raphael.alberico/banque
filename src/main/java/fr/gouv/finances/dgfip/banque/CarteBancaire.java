package fr.gouv.finances.dgfip.banque;

import java.util.Calendar;
import java.util.Date;

public class CarteBancaire extends CompteBancaire {
    // *********** FIELDS
    public String codePin;
    public String numCarte;
    public Date Expiration;

    private Banque banque;
    private CompteCourant compteCourant;

    // *********** CTOR
    public CarteBancaire(String codePin, String numCarte) {
        this.setCodePin(codePin);
        this.numCarte = numCarte;
//        this.Expiration = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.YEAR, 3);
        Date newDate = cal.getTime();
        this.Expiration = newDate;

    }

    // *********** SETTERS/GETTERS
    public Banque getBanque() {
        return banque;
    }

    public void setBanque(Banque banque) {
        this.banque = banque;
    }

    public CompteCourant getCompteCourant() {
        return compteCourant;
    }

    public void setCompteCourant(CompteCourant compteCourant) {
        this.compteCourant = compteCourant;
    }

    /**
     * @return the codePin
     */
    public String getCodePin() {
        return codePin;
    }

    /**
     * @param codePin the codePin to set
     */
    public void setCodePin(String codePin) {
        this.codePin = codePin;
    }

    public Boolean verifierPin(String pin) {
        return this.codePin.equals(pin);
    }

    @Override
    public String toString() {
        return "CarteBancaire [numCarte=" + numCarte + ", codePin=" + codePin
                + ", Expiration=" + Expiration + "]";
    }

//    public void setCarteBancaire(String string) {
//        // TODO Auto-generated method stub
//
//    }

//    public static void add(CarteBancaire c) {
//        // TODO Auto-generated method stub
//
//    }

    public String getNumCarte() {
        return numCarte;
    }

//    public String CarteBancaire1(String CodePin, String numCarte,
//            String Expiration) {
//        return "CarteBancaire [numCarte=" + numCarte + ", codePin=" + CodePin
//                + ", Expiration=" + Expiration + "]";
//    }
}