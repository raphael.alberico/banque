package fr.gouv.finances.dgfip.banque;

public class CompteEpargne extends CompteBancaire {

    protected double tauxInteret;

    public CompteEpargne(String codeBanque, String codeGuichet,
            double soldeInitial, double tauxInteret) {
        this.codeGuichet = codeGuichet;
        this.solde = soldeInitial;
        this.codeBanque = codeBanque;
        this.tauxInteret = tauxInteret;
        this.cle = "999";

    }

    @Override
    public int creerOperation(String libelle, Double montant) throws Exception {
        if (this.solde + montant < 0) {
            throw new Exception("Pas possible");
        } else {
            return super.creerOperation(libelle, montant);
        }
    }

    @Override
    public String toString() {
        return "CompteEpargne [titulaire=" + this.getTitulaire()
                + ", tauxInteret=" + tauxInteret + ", codeBanque=" + codeBanque
                + ", codeGuichet=" + codeGuichet + ", numCompte=" + numCompte
                + ", cle=" + cle + ", solde=" + solde + "]";
    }

}