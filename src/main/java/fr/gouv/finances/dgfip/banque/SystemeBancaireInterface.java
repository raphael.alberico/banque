package fr.gouv.finances.dgfip.banque;

public interface SystemeBancaireInterface {
    public String rechercherRIBCompteCarte(String numCarte)
            throws SystemeBancaireException;

    public Integer creerOperation(String ribCompte, String libelle,
            Double montant) throws SystemeBancaireException;

    public Boolean verifierCodePin(String numCarte, String codePin)
            throws SystemeBancaireException;

}
