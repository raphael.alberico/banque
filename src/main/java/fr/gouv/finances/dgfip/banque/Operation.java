package fr.gouv.finances.dgfip.banque;

import java.util.Date;

public class Operation {
    protected Integer numOperation;
    protected Date dateOperation;
    protected String libelle;
    protected Double montant;

    public Operation(String libelle, Double montant2) {
        this.libelle = libelle;
        this.montant = montant2;
        this.dateOperation = new Date();
    }

    public Integer getNumOperation() {
        return numOperation;
    }

    public void setNumOperation(Integer numOperation) {
        this.numOperation = numOperation;
    }

}
