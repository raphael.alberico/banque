package fr.gouv.finances.dgfip.banque;

public class Personne {
    protected String nom;
    protected String prenom;

    public String getNom() {
        return nom;
    }

    /**
     * il s'agit d'une documentation test
     * 
     * @param nom
     */

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    @Override
    public String toString() {
        return "Personne [nom=" + nom + ", prenom=" + prenom + "]";
    }

}
